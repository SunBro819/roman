# Roman

![Hackerman](https://pbs.twimg.com/profile_images/1035079978008948737/NNtdoxpw_400x400.jpg)

## Description
__FR__ - Ce projet permet de convertir des nombres arabes en un nombre Romain.

__AN__ - This project can transform a arabic number into roman one.

## Statut
En Cours de developpement
Allez sur le site http://lesite.com

## Documentation
README.md

### exemple:
````bash
3 -> III
4 -> IV
````
```mermaid
graph LR
A[Début] -- chiffre = 3  --> B{chiffre < 4 }
B -- chiffreRomain --> C[for =0 i < chffre i++]
C -- chiffreRomain += I  --> C
C -- chiffreRomaine = III --> D[Fin]
```
## Structure

-   `src/app`
-   `config`
-   `bin`


Clone with HTTPS: [https://gitlab.com/JalilArf/roman.git](https://gitlab.com/JalilArf/roman.git)

1. Clone the repository.

```shell
$ git clone https://gitlab.com/<username>/roman.git
```

2. Install dependencies.

```shell
$ cd roman
$ npm install
```

3. Install vscode extensions.

Install the recommended extensions via the extension tab.

4. Build and run the project.

Double-clic on index.js

clone projet
```
$ git clone [URL]
```
en premier installer node.js
```
$ npm init
```
ensuite installer Jest
```
$ npm install jest
```

Allez sur le site http://lesite.com

## Auteurs
* Creator: Jalil Arfaoui
### Contribution
* guillaume dax
* Killian
* Claudian CAMUS
* aleksandrvassilyev95
* Steven Ladowichx
* Pierre Tubeuf
* Nicolas Glories
* Retro Gaming Test
* Elise
* Lisson
* nona tux
* Molinié Clément
* Stephen Soupart
* Loic

This project is developped by the students of ESN81.
